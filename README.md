# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: ARTEM KOLESNIKOV

**E-MAIL**: tema58-rus@yandex.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# PUBLIC KEY

https://yadi.sk/i/hiSeSmwB1sMH9Q